const express = require("express");

const app = new express();

const port = 8000;

app.use((req, res, next) => {
    console.log(new Date());
    next();
})

app.use((req, res, next) => {
    console.log(`Method: ${req.method}`);
    next();
})

app.get(`/hello`, (req, res) => {
    res.status(200).json({
        message: `Hello Devcamp120`,
    })
})

app.get(`/`, (req, res) => {
    let today = new Date();
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()}`,
    })
})

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})
